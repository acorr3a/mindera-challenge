# mindera-challenge

## Getting Started

This source code is developed in python, using selenium and behave to run tests. Docker was used as container for the test scripts.


### Running

The item list application must be running before next steps.

Download project and in root folder, open prompt and run:

```
docker build -t mindera-challenge .
docker run -ti -w /behave/test mindera-challenge behave --tags=@done
```

## Aditional Comments

* This docker image is not working properly, there is some issues with chrome webdriver path and is not possible to run tests automatically. 