FROM python:3.7-slim-stretch

ENV CHROME_DRIVER_VERSION 2.45
ENV CHROME_DRIVER_SOURCE http://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip

COPY . /behave

ENV REQUIREMENTS_PATH /behave/requirements.txt

WORKDIR /behave/test

RUN apt-get update && apt-get install -y --no-install-recommends apt

RUN echo "Install packages in base image to run this script properly..."   && \
    apt-get install -y --no-install-recommends \
        gnupg   \
        dirmngr \
        wget    \
        ca-certificates && \
        rm -rf /var/lib/apt/lists/*  	
		
		
RUN	echo "Add Google Chrome repository..."   && \
    wget -q -O- https://dl.google.com/linux/linux_signing_key.pub | apt-key add -  && \
    echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | tee /etc/apt/sources.list.d/google.list  

# Install some more files	
RUN	apt-get update            && \
    apt-get install -y --no-install-recommends \
        python3-dev              \
        python3-pip              \
        xvfb                     \
        google-chrome-stable		&& \
        rm -rf /var/lib/apt/lists/*  

RUN	echo "Install project modules specified in requirements file..."   && \
	pip3 install -r $REQUIREMENTS_PATH
	

RUN	echo "Install ChromeDriver..." && \	
    wget -qO- $CHROME_DRIVER_SOURCE | zcat > /usr/local/bin/chromedriver  && \
    chown -R root:root /usr/local/bin/chromedriver  && \
    chmod 755 /usr/local/bin/chromedriver
	
RUN export PATH=$PATH:/usr/local/bin/chromedriver
	
RUN ln -s /usr/local/bin/chromedriver /usr/bin