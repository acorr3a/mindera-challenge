from behave import given, when, then
from hamcrest import assert_that, equal_to

@given('I am on item list page')
def step_impl(context):
    context.page.open(context.target_url)

@when('I drag and drop "{a}" over "{b}"')
def step_impl(context, a, b):

    context.page.wait('LIST_ITEMS')
    position_before_drag_and_drop(context, b)
    context.page.drag_and_drop(a, b)

@when('I sort items from minor to major')
def step_impl(context):

    context.page.wait('LIST_ITEMS')
    initial_list = context.page.get_list_items()
    context.sorted_list = sorted(initial_list)

    rearrange(context.page, initial_list, context.sorted_list)

@then('the list should end in ascending order')
def step_impl(context):
    current_list = context.page.get_list_items()
    assert_that(current_list, equal_to(context.sorted_list))

@then('item "{a}" should end in place of "{b}"')
def step_impl(context, a, b):
    assert_that(check_a_before_b(context, a, context.pos_b))

def check_a_before_b(context, a, b):
    elements = context.page.get_list_items()

    pos_a = elements.index(a)

    # check if a was moved to the position where b was
    # new a position == old b position
    if pos_a == b:
        return True
    else:
        return False

def position_before_drag_and_drop(context, b):
    elements = context.page.get_list_items()

    context.pos_b = elements.index(b)

def rearrange(driver, list_unsorted, list_sorted):
    unsorted_start = 0

    while list_unsorted != list_sorted:
        if list_sorted[unsorted_start] != list_unsorted[unsorted_start]:
            source = list_sorted[unsorted_start]
            dest = list_unsorted[unsorted_start]
            driver.drag_and_drop(source,dest)
            list_unsorted = driver.get_list_items()
        else:
            unsorted_start += 1