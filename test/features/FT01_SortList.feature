Feature: Sort List

@done
Scenario Outline: Drag and drop element with success
  Given I am on item list page
  When  I drag and drop "<a>" over "<b>"
  Then  item "<a>" should end in place of "<b>"

  Examples: items
    |    a    |    b   |
    | Item 1  | Item 5 |

@done
Scenario: Sort list in ascending order
  Given I am on item list page
  When  I sort items from minor to major
  Then  the list should end in ascending order