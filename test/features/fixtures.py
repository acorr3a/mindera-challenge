from behave import fixture
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from pages.home_page import HomePage

@fixture
def browser_chrome(context, timeout=30, **kwargs):
    # To start chrome headless, just do

    CHROMEDRIVER_PATH = '/usr/local/bin/chromedriver'

    options = Options()
    options.binary_location = '/usr/bin/google-chrome'
    options.headless = True

    print("Setting chrome webdriver headless")

    context.driver = webdriver.Chrome(chrome_options=options, executable_path=CHROMEDRIVER_PATH)

    # To start chrome default, just do

    # CHROMEDRIVER_PATH = context.chrome_driver_path
    # context.driver = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH)

    # Saves main interaction page in context
    context.page = HomePage(context)
    yield context.page