from behave import use_fixture
from fixtures import browser_chrome

def before_all(context):
    print("Executing behave before_all")

    # Loading behave.ini configs to context object
    context.browser = context.config.userdata['browser']
    context.target_url = context.config.userdata['target_url']

    # Chrome support only, expand browsers in next build
    if context.browser == "chrome":
        use_fixture(browser_chrome, context)
    else:
        print("Invalid browser!")

def after_all(context):
    print("Executing behave after_all")

    context.driver.quit()