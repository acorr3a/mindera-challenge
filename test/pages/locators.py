from selenium.webdriver.common.by import By

class MainPageLocators(object):

    items = {
        'ITEM_0':"//li[contains(text()," + "'Item 0'" + ")]",
        'ITEM_1':"//li[contains(text()," + "'Item 1'" + ")]",
        'ITEM_2':"//li[contains(text()," + "'Item 2'" + ")]",
        'ITEM_3':"//li[contains(text()," + "'Item 3'" + ")]",
        'ITEM_4':"//li[contains(text()," + "'Item 4'" + ")]",
        'ITEM_5':"//li[contains(text()," + "'Item 5'" + ")]",
        'ITEMS':"//li[contains(text()," + "'Item'" + ")]",
        'LIST_ITEMS':"//ul"
    }

    def get_real_name(item_name):
        source_name = item_name.replace(' ', '_').upper()
        return MainPageLocators.items.get(source_name)

    def get_element(driver,item_name):
        source_name = item_name.replace(' ', '_').upper()
        return driver.find_element(By.XPATH, MainPageLocators.items.get(source_name))