from pages.base_page_object import BasePage
from pages.locators import MainPageLocators


class HomePage(BasePage):

    locator_dictionary = MainPageLocators.items

    def __init__(self, context):
        BasePage.__init__(
            self,
            context.driver)

    def get_list_items(self):
        string_elements = []
        elements = self.find_elements('ITEMS')
        for element in elements:
            string_elements.append(element.text)
        return string_elements