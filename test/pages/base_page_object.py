from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.locators import MainPageLocators

class BasePage(object):
    __TIMEOUT = 30

    def __init__(self, browser):
        self.browser = browser
        self._web_driver_wait = WebDriverWait(browser, BasePage.__TIMEOUT)

    def open(self, url):
        self.browser.get(url)

    def find_element(self, element_name):
        return self.browser.find_element(By.XPATH, MainPageLocators.get_real_name(element_name))

    def find_elements(self, element_name):
        return self.browser.find_elements(By.XPATH, MainPageLocators.get_real_name(element_name))

    def wait(self, element_name):
        return self._web_driver_wait.until(EC.visibility_of_element_located((By.XPATH, MainPageLocators.get_real_name(element_name))))

    def drag_and_drop(self, source_ele, dest_ele):
        ActionChains(self.browser).drag_and_drop(MainPageLocators.get_element(self.browser,source_ele),
                                                 MainPageLocators.get_element(self.browser,dest_ele)).perform()